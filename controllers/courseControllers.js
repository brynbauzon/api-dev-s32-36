const Course = require('./../models/Course');
const User = require('./../models/User');


//To get all course
module.exports.getAllCourses = () => {
	//difference between findOne() & find()
			//findOne() returns one document
			//find() returns an array of documents []

	return User.find().then( (result, error) => {
		if(result !== null){
			return result
		} else {
			return error
		}
	})
}

// To create a course
module.exports.createCourse = (reqBody) => {

	
		let newCourse = new Course({

		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
			
	});

	//save the newCourse object to  the database
	return newCourse.save().then( (result, error) => {
		//console.log(result)	//document
		if(result){
			return true
		} else {
			return error
		}
	})
}

module.exports.enroll = async (data) =>{

  const {userId, courseId} = data; 


  const userEnroll = await User.findById(userId).then((result, error)=>{
  	if(error){

  		return error	
  	}else{

  		result.enrollments.push({courseId: courseId});
  		return result.save().then(result => true)
  	}

  })

  const courseEnroll = await Course.findById(courseId).then((result, error)=>{

  	if(error){

  		return error
  	}else{


  		result.enrollees.push({userId: userId})

  		return result.save().then(result => {

  			return ture
  		})
  	}

  })


  if(userEnroll && courseEnroll){

  	return true
  }else{

  	return false
  }


}